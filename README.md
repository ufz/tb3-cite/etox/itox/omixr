# omixR

This is a collection of R functions to predict toxicogenomic fingerprints of mixtures.

To install the package, you have to install the package devtools first:

```r
install.packages("devtools")
```

Afterwards you should install toxprofileR to retrieve single substance toxicogenomic fingerprints:

```r
devtools::install_git("https://git.ufz.de/itox/toxprofileR.git")
```

Finally, you are ready to install the omixR package

```r
devtools::install_git("https://git.ufz.de/itox/omixR.git")
```


Please note: There might be difficulties installing dependencies via Rstudio. If problems arise, please directly install package from R.