#' Mixture prediction for one node
#'
#' @param nodeID
#' @param params_CA_hill
#' @param params_IA_hill
#' @param parambounds
#' @param fractions
#' @param doseseq
#' @param timepoints
#' @param AIC_threshold
#' @param effect_ratio_threshold
#'
#' @return
#' @export
#'
predict_node <- function(nodeID, params_CA_hill, params_IA_hill, parambounds, fractions, doseseq, timepoints,AIC_threshold = 0.9,effect_ratio_threshold = 0.2){

if(!is.na(params_CA_hill[[nodeID]][[1]]$hillslope[1])){
CA_node <- omixR::predict_CA_hill(params_CA_hill_node = params_CA_hill[[nodeID]],
                                  fractions = fractions,
                                  doseseq = doseseq,
                                  timepoints = timepoints,
                                  AIC_threshold = AIC_threshold,
                                  effect_ratio_threshold = effect_ratio_threshold)

IA_node <- omixR::predict_IA_hill(params_IA_hill_node = params_IA_hill[[nodeID]],
                                  fractions = fractions,
                                  doseseq = doseseq,
                                  timepoints = timepoints,
                                  AIC_threshold = AIC_threshold,
                                  effect_ratio_threshold = effect_ratio_threshold)

EA_node <- omixR::predict_EA_hill(params_IA_hill[[nodeID]],
                                  fractions = fractions,
                                  doseseq = doseseq,
                                  timepoints = timepoints,
                                  AIC_threshold = AIC_threshold,
                                  effect_ratio_threshold = effect_ratio_threshold)

fit_CA <- omixR::fit_predictions(nodedf = CA_node$CA,
                                 parambounds = parambounds,
                                 maxr = params_CA_hill[[nodeID]][[1]]$maxGene[1])

fit_IA <- omixR::fit_predictions(nodedf = IA_node$IA,
                                 parambounds = parambounds,
                                 maxr = params_IA_hill[[nodeID]][[1]]$maxGene[1])

fit_EA <- omixR::fit_predictions(nodedf = EA_node$EA,
                                 parambounds = parambounds,
                                 maxr = params_IA_hill[[nodeID]][[1]]$maxGene[1])

resultframe <- rbind(CA_node$ECfracs_all_CA,
                     IA_node$ECfracs_all_IA,
                     EA_node$EC_fracs_all_EA,
                     CA_node$CA,
                     IA_node$IA,
                     EA_node$EA
)

nodesum <-
    aggregate.data.frame(
        resultframe$logFC,
        by = list(
            resultframe$time,
            resultframe$mixtureconc,
            resultframe$substance,
            resultframe$model
        ),
        quantile,
        probs = c(0.05, 0.125, 0.25, 0.5, 0.75, 0.875, 0.95)
    )

nodesum <- cbind(nodesum[, -5], as.data.frame(nodesum$x))
colnames(nodesum) <-
    c(
        "time",
        "conc",
        "substance",
        "type",
        "Q05",
        "Q12_5",
        "Q25",
        "Q50",
        "Q75",
        "Q87_5",
        "Q95"
    )

nodesum$substance <- as.character(nodesum$substance)
nodesum$time <- as.numeric(nodesum$time)
nodesum$type <- as.character(nodesum$type)
nodesum$node <- nodeID

nodesum_final <-
    list(
        predict_df = nodesum,
        fit_CA = c(fit_CA, node = nodeID),
        fit_IA = c(fit_IA, node = nodeID),
        fit_EA = c(fit_EA, node = nodeID)
    )

message("Finished node #", nodeID)
return(nodesum_final)
}
}
