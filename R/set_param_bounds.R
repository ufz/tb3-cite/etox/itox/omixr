#' Define parameter boundaries for mobi and biphi CTR based on experimental concentrations and time points
#'
#' @param designconcentrations
#' @param timepoints
#'
#' @return a list containing parameter boundaries
#' @export
#'
set_param_bounds <- function(designconcentrations, designtimes){

concentrations <- sort(unique(designconcentrations[designconcentrations != 0]), decreasing = T)
dilution_factor <- min(exp(diff(log(concentrations[order(concentrations)]))), na.rm = T)
concrange <- max(concentrations) / min(concentrations)

timepoints <- sort(unique(designtimes), decreasing = T)
time_factor <- min(exp(diff(log(timepoints[order(timepoints)]))), na.rm = T)
timerange <- max(timepoints) / min(timepoints)



list(
    slope = c(
        min = -log((1 / 0.505) - 1) / log(concrange^(0.5)),
        max = -log((1 / 0.99) - 1) / log(dilution_factor^0.5)
    ),
    sigma = c(
        min = log(time_factor) / ((-2 * log(0.01))^0.5),
        max = log(timerange) / ((-2 * log(0.99))^0.5)
    ),
    EC50 = c(
        min = min(concentrations) / concrange,
        max = max(concentrations) * concrange
    ),
    mS50 = c(
        min = 1 / (max(concentrations) * concrange),
        max = 1 / (min(concentrations) / concrange)
    ),
    mu = c(
        min = min(timepoints, na.rm = T)/2,
        max = max(timepoints, na.rm = T)+3
    ),
    mconc = c(
        min = min(concentrations) / concrange,
        max = max(concentrations) * concrange
    ),
    sconc = c(
        min = log(dilution_factor) / ((-2 * log(0.01))^0.5),
        max = log(concrange) / ((-2 * log(0.99))^0.5)
    ),
    err = c(
        min = 0,
        max = 10
    )
)
}
