#' Predict CIdiffs for Mixture
#'
#' @param nodelist_list list of nodelists of model components
#' @param mixpredict mixture prediction
#' @param designtimes designtimes
#' @param designconcentrations designconcentrations
#' @param predictmodel CA, IA, or EA
#'
#' @return
#' @export
#'
predict_CIdiffs <-
    function(nodelist_list,
             mixpredict,
             designtimes,
             designconcentrations,
             predictmodel = c("CA", "IA", "EA")) {
        overlap_tcta <- lapply(
            X = seq_len(length(nodelist_list[[1]])),
            FUN = function(nodeID) {
                nodedf <-
                    do.call("rbind", lapply(nodelist_list, function(nodelist) {
                        nodelist[[nodeID]]
                    }))
                if (dim(nodedf)[2] > 1) {
                    treatmentns <-
                        nrow(nodedf[nodedf$concentration_umol_l != 0,])

                    controls <-
                        nodedf[nodedf$concentration_umol_l == 0,]
                    controlns <-
                        aggregate(controls$logFC,
                                  by = list(time_hpe = controls$time_hpe),
                                  length)

                    # take quantiles --------------------------------------------------------
                    ControlCIs <-
                        aggregate(
                            controls$logFC,
                            by = list(time_hpe = controls$time_hpe),
                            quantile,
                            c(0.025, 0.975)
                        )
                    ControlCIs <-
                        cbind(ControlCIs$time_hpe,
                              as.data.frame(ControlCIs$x))
                    colnames(ControlCIs) <-
                        c("time_hpe", "min_hill", "max_hill")
                    ControlCIs$n <- controlns$x
                    ControlCIs$max_gauss <- ControlCIs$max_hill
                    ControlCIs$min_gauss <- ControlCIs$min_hill

                    # calculate confidence interval for predictions ---------------------------
                    modeldf <-
                        expand.grid(time_hpe = designtimes,
                                    concentration_umol_l = designconcentrations)


                    modeldf$logFC_hill <- NA
                    modeldf$logFC_gauss <- NA
                    modeldf$n <- treatmentns

                    if (predictmodel == "CA") {
                        modelpredict <- mixpredict$fit_CA
                    }

                    if (predictmodel == "IA") {
                        modelpredict <- mixpredict$fit_IA
                    }

                    if (predictmodel == "EA") {
                        modelpredict <- mixpredict$fit_EA
                    }

                    # hill-gauss ------------------------------------------------------------
                    modeldf$logFC_hill <-
                        toxprofileR::hill_gauss(
                            dose = modeldf$concentration_umol_l,
                            time = modeldf$time_hpe,
                            hillslope = modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "hillslope_best_hill"],
                            maxS50 = modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "maxS50_best_hill"],
                            mu = modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "mu_best_hill"],
                            sigma = modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "sigma_best_hill"],
                            maxGene = modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "max_best_hill"]
                        )

                    modeldf$se_hill <-
                        modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "err_best_hill"]
                    modeldf$max_hill <-
                        modeldf$logFC_hill + (qnorm(0.975) * modeldf$se_hill)
                    modeldf$min_hill <-
                        modeldf$logFC_hill - (qnorm(0.975) * modeldf$se_hill)



                    # gauss-gauss ------------------------------------------------------------
                    modeldf$logFC_gauss <-
                        toxprofileR::gauss_gauss(
                            dose = modeldf$concentration_umol_l,
                            time = modeldf$time_hpe,
                            mconc = modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "mconc_best_gauss"],
                            sconc = modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "sconc_best_gauss"],
                            mu = modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "mu_best_gauss"],
                            sigma = modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "sigma_best_gauss"],
                            maxGene = modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "max_best_gauss"]
                        )

                    modeldf$se_gauss <-
                        modelpredict[modelpredict$node == nodeID&!is.na(modelpredict$node), "err_best_gauss"]
                    modeldf$max_gauss <-
                        modeldf$logFC_gauss + (qnorm(0.975) * modeldf$se_gauss)
                    modeldf$min_gauss <-
                        modeldf$logFC_gauss - (qnorm(0.975) * modeldf$se_gauss)


                    # calculate difference for each measured treatment -----------------------

                    # hill-gauss -----
                    modeldf$diff_hill <-
                        apply(
                            modeldf,
                            MARGIN = 1,
                            FUN = function(treatment) {
                                if (as.numeric(treatment["min_hill"]) > 0) {
                                    dif <-
                                        as.numeric(treatment["min_hill"]) - ControlCIs$max_hill[ControlCIs$time_hpe ==
                                                                                                    as.numeric(treatment["time_hpe"])]
                                    if (length(dif) == 0) {
                                        dif <- 0
                                    } # if controls were removed as outliers
                                    dif[dif < 0] <- 0
                                    return(dif)
                                } else {
                                    if (as.numeric(treatment["max_hill"]) < 0) {
                                        dif <-
                                            as.numeric(treatment["max_hill"]) - ControlCIs$min_hill[ControlCIs$time ==
                                                                                                        as.numeric(treatment["time_hpe"])]
                                        if (length(dif) == 0) {
                                            dif <- 0
                                        } # if controls were removed as outliers
                                        dif[dif > 0] <- 0
                                        return(dif)
                                    } else {
                                        return(0)
                                    }
                                }
                            }
                        )

                    # gauss-gauss ------
                    modeldf$diff_gauss <-
                        apply(
                            modeldf,
                            MARGIN = 1,
                            FUN = function(treatment) {
                                if (as.numeric(treatment["min_gauss"]) > 0) {
                                    dif <-
                                        as.numeric(treatment["min_gauss"]) - ControlCIs$max_gauss[ControlCIs$time_hpe ==
                                                                                                      as.numeric(treatment["time_hpe"])]
                                    if (length(dif) == 0) {
                                        dif <- 0
                                    } # if controls were removed as outliers
                                    dif[dif < 0] <- 0
                                    return(dif)
                                } else {
                                    if (as.numeric(treatment["max_gauss"]) < 0) {
                                        dif <-
                                            as.numeric(treatment["max_gauss"]) - ControlCIs$min_gauss[ControlCIs$time ==
                                                                                                          as.numeric(treatment["time_hpe"])]
                                        if (length(dif) == 0) {
                                            dif <- 0
                                        } # if controls were removed as outliers
                                        dif[dif > 0] <- 0
                                        return(dif)
                                    } else {
                                        return(0)
                                    }
                                }
                            }
                        )

                    # add controls -----
                    ControlCIs$concentration_umol_l <- 0
                    ControlCIs$logFC_hill <- 0
                    ControlCIs$diff_hill <- NA
                    ControlCIs$se_hill <- NA
                    ControlCIs$logFC_gauss <- 0
                    ControlCIs$diff_gauss <- NA
                    ControlCIs$se_gauss <- NA

                    modeldf <- rbind(modeldf, ControlCIs)

                    modeldf$node <- nodeID

                    return(modeldf)
                } else {
                    return(NA)
                }
            }
        )


        return(overlap_tcta)

    }


#' predict CIdiff component
#'
#' @param concentrations
#' @param timepoints
#' @param tcta_paramframe
#' @param CIdiff
#'
#' @return
#' @export
#'
predict_CIdiff_component <- function(concentrations, timepoints, tcta_paramframe, CIdiff){

    treatments <- expand.grid(concentrations=concentrations, timepoints=timepoints)

    CIdiffs_pred <- apply(treatments, 1, function(treatment){

        concentration_umol_l <- treatment["concentrations"]
        time_hpe <- treatment["timepoints"]




        plotdata <- data.frame(
            logFC = toxprofileR::hill_gauss(
                dose = concentration_umol_l,
                time = time_hpe,
                hillslope = as.numeric(tcta_paramframe[, "hillslope_best_hill"]),
                maxS50 = as.numeric(tcta_paramframe[, "maxS50_best_hill"]),
                mu = as.numeric(tcta_paramframe[, "mu_best_hill"]),
                sigma = as.numeric(tcta_paramframe[, "sigma_best_hill"]),
                maxGene = as.numeric(tcta_paramframe[, "max_best_hill"])
            ),
            dose = concentration_umol_l,
            time = time_hpe,
            x = grid$pts[, 1],
            y = grid$pts[, 2],
            node = 1:3600
        )


        plotdata$maxControl <- unlist(lapply(CIdiff, function(nodediff){
            if(is.data.frame(nodediff)){
                nodediff[nodediff[,"time_hpe"]==time_hpe&nodediff[,"concentration_umol_l"]==0,"max_hill"]
            }else{NA}
        }))

        plotdata$minControl <- unlist(lapply(CIdiff, function(nodediff){
            if(is.data.frame(nodediff)){
                nodediff[nodediff[,"time_hpe"]==time_hpe&nodediff[,"concentration_umol_l"]==0,"min_hill"]
            }else{NA}
        }))


        plotdata$upr_hill <-
            plotdata$logFC + (qnorm(0.95) * as.numeric(tcta_paramframe[, "err_best_hill"]))
        plotdata$lwr_hill <-
            plotdata$logFC - (qnorm(0.95) * as.numeric(tcta_paramframe[, "err_best_hill"]))

        plotdata$CIdiff <- 0


        plotdata$CIdiff <-
            apply(
                plotdata,
                MARGIN = 1,
                FUN = function(treatment) {
                    if(!is.na(treatment["maxControl"])){
                        if (as.numeric(treatment["lwr_hill"]) > 0) {
                            dif <-
                                as.numeric(treatment["lwr_hill"]) - as.numeric(treatment["maxControl"])
                            dif[dif < 0] <- 0
                            return(dif)
                        } else {
                            if (as.numeric(treatment["upr_hill"]) < 0) {
                                dif <-
                                    as.numeric(treatment["upr_hill"]) - as.numeric(treatment["minControl"])
                                dif[dif > 0] <- 0
                                return(dif)
                            } else {
                                return(0)
                            }
                        }
                    } else {NA}
                }
            )
        return(plotdata)
    })

    CIdiffs_pred <- do.call("rbind", CIdiffs_pred)

    return(CIdiffs_pred)

}
