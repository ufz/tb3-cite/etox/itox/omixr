#' Predict Mixture Fingerprint
#'
#' @param tcta_list
#' @param sl_list
#' @param fractions
#' @param designconcentrations
#' @param designtimes
#' @param nsample
#' @param doseseq
#' @param cluster
#' @param cln
#' @param AIC_threshold
#' @param effect_ratio_threshold
#'
#' @return
#' @export
#'
predict_mix_fingerprint <-
    function(tcta_list,
             sl_list,
             fractions,
             designconcentrations,
             designtimes,
             nsample,
             doseseq,
             AIC_threshold = 0.9,
             effect_ratio_threshold = 0.2,
             cluster = c(T,F),
             cln = 2) {
        library("snow")

        stopifnot(names(tcta_list) == names(sl_list))

        tcta_sl_list <- Map(cbind, tcta_list, sl_list)
        tcta_sl_list <-
            lapply(tcta_sl_list, function(tcta_paramframe) {
                colnames(tcta_paramframe) <-
                    gsub("dots.*", "sl", colnames(tcta_paramframe))
                tcta_paramframe
            })

        CAdirs <- omixR::define_CA_direction(tcta_list = tcta_list)


        # retrieve model parameters for selected direction ------------------------------
        ## CA --------------------------------------------------------------------------
        params_CA_hill <-
            omixR::get_CA_params_hill(tcta_sl_list = tcta_sl_list,
                               CAdirs = CAdirs,
                               nsample = nsample,
                               fractions = fractions)

        # params_CA_gauss <-
        #     omixR::get_CA_params_gauss(tcta_sl_list = tcta_sl_list,
        #                         CAdirs = CAdirs,
        #                         nsample = nsample)

        ## IA ----------------------------------------------------------------------------
        params_IA_hill <-
            omixR::get_IA_params_hill(tcta_sl_list = tcta_sl_list, nsample = nsample, fractions = fractions)
        # params_IA_gauss <-
        #     omixR::get_IA_params_gauss(tcta_sl_list = tcta_sl_list, nsample = nsample)


        # mixture prediction sampling -------------------------------------------------------------------
        parambounds <- omixR::set_param_bounds(designconcentrations = designconcentrations,
                                                designtimes = timepoints)


        #nodeID <- 1062


        if (cluster) {
                # check for external cluster --------------------------------------------------
                cl <- snow::getMPIcluster()

                # if not available set up cluster ---------------------------------------------
                if (is.null(cl)) {
                    clustertype <- 1
                    cl <- snow::makeMPIcluster(cln, type = "SOCK")
                } else {
                    clustertype <- 0
                }

                # load libraries on cluster ---------------------------------------------------
                snow::clusterEvalQ(cl, expr = library("hydromad"))
                snow::clusterEvalQ(cl, expr = library("mgcv"))
                snow::clusterEvalQ(cl, expr = library("omixR"))

                # load data on cluster --------------------------------------------------------
                # snow::clusterExport(cl, c("nodelist_extrema", "param_bounds"))

                # apply modeling function -----------------------------------------------------
                tictoc::tic()
                mix_predictions <- snow::parLapply(cl = cl,
                                                      x = seq_along(tcta_sl_list[[1]][,1]),
                                                      fun = omixR::predict_node,
                                                      params_CA_hill = params_CA_hill,
                                                      params_IA_hill = params_IA_hill,
                                                      parambounds = parambounds,
                                                      fractions = fractions,
                                                      doseseq = doseseq,
                                                      timepoints = timepoints,
                                                      AIC_threshold = AIC_threshold,
                                                      effect_ratio_threshold = effect_ratio_threshold)
                tictoc::toc()

            } else {
                # apply modeling without paralellization
                tictoc::tic()
                mix_predictions <- lapply(X = seq_along(tcta_sl_list[[1]][,1]),
                                                        FUN = omixR::predict_node,
                                                        params_CA_hill = params_CA_hill,
                                                        params_IA_hill = params_IA_hill,
                                                        parambounds = parambounds,
                                                        fractions = fractions,
                                                        doseseq = doseseq,
                                                        timepoints = timepoints,
                                                        AIC_threshold = AIC_threshold,
                                                        effect_ratio_threshold = effect_ratio_threshold)
                tictoc::toc()
            }

            predict_df <- as.data.frame(do.call("rbind", lapply(mix_predictions, function(x){x$predict_df})))
            fit_CA <- as.data.frame(do.call("rbind", lapply(mix_predictions, function(x){x$fit_CA})))
            fit_IA <- as.data.frame(do.call("rbind", lapply(mix_predictions, function(x){x$fit_IA})))
            fit_EA <- as.data.frame(do.call("rbind", lapply(mix_predictions, function(x){x$fit_EA})))

            ## add NA nodes to paramframe to make it work with toxprofileR package ------------------
            nodeadd <- c(1:3600)[!c(1:3600)%in%fit_CA$node]
            for(node in nodeadd){
                toadd <- c(rep(NA,(length(colnames(fit_CA))-1)), node)
                names(toadd) <- colnames(fit_CA)
                fit_CA <- rbind(fit_CA,toadd)
            }

            nodeadd <- c(1:3600)[!c(1:3600)%in%fit_IA$node]
            for(node in nodeadd){
                toadd <- c(rep(NA,(length(colnames(fit_IA))-1)), node)
                names(toadd) <- colnames(fit_IA)
                fit_IA <- rbind(fit_IA,toadd)
            }

            nodeadd <- c(1:3600)[!c(1:3600)%in%fit_EA$node]
            for(node in nodeadd){
                toadd <- c(rep(NA,(length(colnames(fit_EA))-1)), node)
                names(toadd) <- colnames(fit_EA)
                fit_EA <- rbind(fit_EA,toadd)
            }

            fit_CA <- fit_CA[order(fit_CA$node, decreasing = F),]
            fit_IA <- fit_IA[order(fit_IA$node, decreasing = F),]
            fit_EA <- fit_EA[order(fit_EA$node, decreasing = F),]



            return(list(predict_df = predict_df, fit_CA = fit_CA, fit_IA = fit_IA, fit_EA = fit_EA, params_CA_hill = params_CA_hill, params_IA_hill = params_IA_hill, fractions = fractions, nsample = nsample))
    }



